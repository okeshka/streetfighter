import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  const attr = { tagName: 'span', className: 'fighter-preview__name'} ;
  const elem = Array(4).fill(1).map(()=>createElement(attr));
  const elemDetails = [createFighterImage(fighter), ...elem];
  const [, fighterName, fighterHealth, fighterDefence, fighterAttack] = elemDetails;
  fighterName.textContent = fighter.name;
  fighterHealth.textContent = "health - " + fighter.health;
  fighterDefence.textContent = "defence - " + fighter.defense;
  fighterAttack.textContent = "attack - " + fighter.attack;
  fighterElement.append(...elemDetails);
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
