import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const indicatorSecond = document.getElementById('right-fighter-indicator');
  const indicatorFirst = document.getElementById('left-fighter-indicator');
  const healthFirst = firstFighter.health;
  const healthSecond = secondFighter.health;
 
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keyup', (e) => pressed.delete(e.code));
    document.addEventListener('keydown', (e) => fightControl(e));

    let pressed = new Set();
    const fightControl = e => {

      pressed.add(e.code);
      
      if (pressed.has(controls.PlayerOneAttack) && pressed.has(controls.PlayerTwoBlock))
      {
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        secondFighter.health = secondFighter.health > 0 ? secondFighter.health : 0;
        indicatorSecond.style.width = `${(secondFighter.health / healthSecond) * 100}%`;
      }
     else if (pressed.has(controls.PlayerOneAttack)) {
        secondFighter.health -= getDamage(firstFighter, null);
        secondFighter.health = secondFighter.health > 0 ? secondFighter.health : 0;
        indicatorSecond.style.width = `${(secondFighter.health / healthSecond) * 100}%`
    }
      if (pressed.has(controls.PlayerTwoAttack) && pressed.has(controls.PlayerOneBlock)) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        firstFighter.health = firstFighter.health > 0 ? firstFighter.health : 0;
        indicatorFirst.style.width = `${(firstFighter.health / healthFirst) * 100}%`
    }
    else if (pressed.has(controls.PlayerTwoAttack)) {
          firstFighter.health -= getDamage(secondFighter, null);
          firstFighter.health = firstFighter.health > 0 ? firstFighter.health : 0;
          indicatorFirst.style.width = `${(firstFighter.health / healthFirst) * 100}%`
      }
    
    if (pressed.has(controls.PlayerOneCriticalHitCombination[0]) && 
        pressed.has(controls.PlayerOneCriticalHitCombination[1]) &&
        pressed.has(controls.PlayerOneCriticalHitCombination[2])) {
        secondFighter.health -= criticalHit(firstFighter);
        secondFighter.health = secondFighter.health > 0 ? secondFighter.health : 0;
        indicatorSecond.style.width = `${(secondFighter.health / healthSecond) * 100}%`
        }
    
    if (pressed.has(controls.PlayerTwoCriticalHitCombination[0]) && 
    pressed.has(controls.PlayerTwoCriticalHitCombination[1]) &&
    pressed.has(controls.PlayerTwoCriticalHitCombination[2])) {
    firstFighter.health -= criticalHit(secondFighter);
    firstFighter.health = firstFighter.health > 0 ? firstFighter.health : 0;
    indicatorFirst.style.width = `${(firstFighter.health / healthFirst) * 100}%`
    }

    if (firstFighter.health <= 0 ) resolve(secondFighter);
    if (secondFighter.health <= 0) resolve(firstFighter);
    console.warn(firstFighter.health, 'vs', secondFighter.health);
    };
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  console.log('защита на', getBlockPower(defender))
  return damage > 0 ? damage : 0; 
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = () =>  Math.random() + 1;
  return fighter.attack * criticalHitChance()
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = () =>  Math.random() + 1;
  return fighter ? fighter.defense * dodgeChance() : 0;  
}

function criticalHit(fighter) {
  return fighter.attack * 2
}