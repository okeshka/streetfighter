import {showModal} from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const message = {
    title : 'The winner is', 
    bodyElement: fighter.name,
    onClose: function() {
      document.location.reload()
    }
  }
  showModal(message);
}
